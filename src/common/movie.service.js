import Vue from 'vue';

const MoviesService = {
  async create(params) {
    const { data } = await Vue.axios.post('movies', params);
    return data;
  },
  async update(id, params) {
    const { data } = await Vue.axios.put(`movies/${id}`, params);
    return data;
  },
  async get(id) {
    const { data } = await Vue.axios.get(`movies/${id}`);
    return data;
  },
  delete(id) {
    return Vue.axios.delete(`movies/${id}`);
  },
  async list(skip = 0, limit = 12) {
    const { data } = await Vue.axios.get('movies/', {
      params: {
        skip,
        limit,
      },
    });
    return data;
  },
};

export default MoviesService;
