import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import { API_URL } from './common/config';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import YearFilter from './common/date.filter';

Vue.config.productionTip = false;
Vue.filter('year', YearFilter);

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = API_URL;

new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app');
