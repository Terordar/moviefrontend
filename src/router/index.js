import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/movies/:id',
    name: 'movie',
    component: () => import('../views/Movie.vue'),
    props: true,
  },
  {
    path: '/editor/:id?',
    name: 'movie-edit',
    props: true,
    component: () => import('../views/MovieEdit.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
